package com.etsonev.unittestdemo.user;

import com.etsonev.unittestdemo.account.Account;
import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class User {
  @Id @GeneratedValue private long id;

  private String firstName;
  private String lastName;
  private String email;

  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Account> accounts = new ArrayList<>();

  public void addAccount(Account account) {
    accounts.add(account);
    account.setUser(this);
  }

  public void removeAccount(Account account) {
    accounts.remove(account);
    account.setUser(null);
  }
}
