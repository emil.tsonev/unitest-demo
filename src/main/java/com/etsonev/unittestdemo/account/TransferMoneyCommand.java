package com.etsonev.unittestdemo.account;

import com.etsonev.unittestdemo.account.money.Money;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class TransferMoneyCommand {
  private long fromAccountId;
  private long toAccountId;
  private Money amount;
}
