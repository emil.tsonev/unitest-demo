package com.etsonev.unittestdemo.account;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;

@Service
@RequiredArgsConstructor
public class AccountService {
  private final AccountRepository accountRepository;

  @Transactional
  public void transferMoney(TransferMoneyCommand transferMoneyCommand) {
    var fromAccount =
        accountRepository
            .findById(transferMoneyCommand.getFromAccountId())
            .orElseThrow(
                () ->
                    new EntityNotFoundException(
                        "Account could not be found by id: " + transferMoneyCommand.getFromAccountId()));

    var toAccount =
        accountRepository
            .findById(transferMoneyCommand.getToAccountId())
            .orElseThrow(
                () ->
                    new EntityNotFoundException(
                        "Account could not be found by id: " + transferMoneyCommand.getToAccountId()));

    fromAccount.withdraw(transferMoneyCommand.getAmount());
    toAccount.deposit(transferMoneyCommand.getAmount());
  }
}
