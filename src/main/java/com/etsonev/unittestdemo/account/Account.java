package com.etsonev.unittestdemo.account;

import com.etsonev.unittestdemo.account.money.Money;
import com.etsonev.unittestdemo.user.User;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Data
public class Account {
  @Id @GeneratedValue private long id;

  private Money amount;

  @ManyToOne(fetch = FetchType.LAZY)
  private User user;

  public void withdraw(Money amount) {
    if (amount.getAmount().compareTo(this.amount.getAmount()) < 0) {
      var subtractedAmount = this.amount.getAmount().subtract(amount.getAmount());
      this.amount = new Money(subtractedAmount);
    } else {
      throw new IllegalStateException("Account could not be widthdrawn to 0 or less money");
    }
  }

  public void deposit(Money amount) {
    var depositedAmount = this.amount.getAmount().add(amount.getAmount());
    this.amount = new Money(depositedAmount);
  }
}
