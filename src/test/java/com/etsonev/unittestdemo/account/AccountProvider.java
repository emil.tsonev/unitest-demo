package com.etsonev.unittestdemo.account;

import com.etsonev.unittestdemo.account.money.Money;

import java.math.BigDecimal;

public class AccountProvider {

  public static Account accountWith100Amount() {
    Money money = new Money(BigDecimal.valueOf(100));
    Account account = new Account();
    account.setId(1L);
    account.setAmount(money);

    return account;
  }

  public static Account accountWith150Amount() {
    Money money = new Money(BigDecimal.valueOf(150));
    Account account = new Account();
    account.setId(2L);
    account.setAmount(money);

    return account;
  }
}
