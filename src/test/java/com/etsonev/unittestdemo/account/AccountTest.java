package com.etsonev.unittestdemo.account;

import com.etsonev.unittestdemo.account.money.Money;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class AccountTest {

  @DisplayName("Given amount should be withdrawn from account if it does not leave the account with 0 or less money")
  @Test
  public void testWithdrawalWithLessMoneyThanTheAccountHas() {
    var account = AccountProvider.accountWith100Amount();

    var money = new Money(BigDecimal.valueOf(99));

    account.withdraw(money);

    assertEquals(BigDecimal.ONE, account.getAmount().getAmount());
  }

  @DisplayName("Given amount that will leave the account with zero or less money exception should be thrown")
  @Test
  public void testAccountCannotBeWithdrawnToZeroOrLessAmount() {
    var account = AccountProvider.accountWith100Amount();
    var money = new Money(BigDecimal.valueOf(100));

    assertThrows(IllegalStateException.class, () -> account.withdraw(money));
  }
}
