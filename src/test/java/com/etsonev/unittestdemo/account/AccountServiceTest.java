package com.etsonev.unittestdemo.account;

import com.etsonev.unittestdemo.account.money.Money;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {
  @Mock
  private AccountRepository accountRepository;

  @InjectMocks
  private AccountService accountService;

  @DisplayName("Transfer of money between two accounts should happen if they can be found and from account is not withdrawn to 0 or less money")
  @Test
  public void testTransferOfMoneyBetweenAccounts() {
    long fromAccountId = 1L;
    long toAccountId = 2L;
    Money money = new Money(BigDecimal.valueOf(50));
    TransferMoneyCommand transferMoneyCommand = new TransferMoneyCommand();
    transferMoneyCommand.setFromAccountId(fromAccountId);
    transferMoneyCommand.setToAccountId(toAccountId);
    transferMoneyCommand.setAmount(money);

    var fromAccount = AccountProvider.accountWith150Amount();
    var toAccount = AccountProvider.accountWith100Amount();
    when(accountRepository.findById(fromAccountId)).thenReturn(Optional.of(fromAccount));
    when(accountRepository.findById(toAccountId)).thenReturn(Optional.of(toAccount));

    accountService.transferMoney(transferMoneyCommand);

    assertEquals(BigDecimal.valueOf(100), fromAccount.getAmount().getAmount());
    assertEquals(BigDecimal.valueOf(150), toAccount.getAmount().getAmount());
  }

  @DisplayName("Transfer of money does not happen if from account can not be found")
  @Test
  public void testNoMoneyIsTransferedIfFromAccountIsNotFound() {
    long fromAccountId = 1L;
    long toAccountId = 2L;
    Money money = new Money(BigDecimal.valueOf(50));
    TransferMoneyCommand transferMoneyCommand = new TransferMoneyCommand();
    transferMoneyCommand.setFromAccountId(fromAccountId);
    transferMoneyCommand.setToAccountId(toAccountId);
    transferMoneyCommand.setAmount(money);

    when(accountRepository.findById(1L)).thenReturn(Optional.empty());

    assertThrows(EntityNotFoundException.class, () -> accountService.transferMoney(transferMoneyCommand));
  }
}
